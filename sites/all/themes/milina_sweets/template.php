<?php

drupal_add_js(path_to_theme().'/js/vebko.js');


function milina_sweets_preprocess_html(&$variables) {
drupal_add_css('http://fonts.googleapis.com/css?family=Dosis:500', array('type' => 'external'));
drupal_add_css('http://fonts.googleapis.com/css?family=Roboto+Condensed', array('type' => 'external'));
}

function milina_sweets_form_alter(&$form, &$form_state, $form_id) {
    if ($form_id == 'contact_site_form') {
        $form['#submit'][] = 'contact_form_submit_handler';
    }
}

function contact_form_submit_handler(&$form, &$form_state) {
$path = current_path();
    $form_state['redirect'] = $path;
}

function manualMetaTags($path) {
$path = current_path();
$path_alias = drupal_lookup_path('alias',$path);

if(drupal_is_front_page()||($path_alias=='jobs')){
  // determine the path of the page
  $path = drupal_get_path_alias($path);

  
  switch ($path) {     
    case "node":
    $url= "http://milinasweets.com'>";
     $title = "Милина Свитс";
        $description = "Компанија за произвотство на кондиторски производи во Штип, Македонија.";
        $keywords    = "milinasweets.com, milina, sweets, milina sweets, милина свитс, кондиторски производи, свитс, Штип, чоколада, фабрика, компанија, kompanija, konditorksi proizvodi";
        $image = "fb.jpg";
        break;
   
    case "node/28":
    $url= "http://milinasweets.com'>";
     $title = "Милина Свитс";
        $description = "За Нашата работа и нашиот менаџерски тим.";
        $keywords    = "Запознајте се со повеќе со нашата компанија и менаџерски тим, менаџерски тим, компанија, за нас,";
        $image = "fb.jpg";
        break;
   
    case "products":
    $url= "http://milinasweets.com'>";
        $description = "Погледнете ја листата на нашите кондиторски производи.";
        $keywords    = "Погледнете ја листата на нашите кондиторски производи.";
        $image = "fb.jpg";
        break;
        
    case "news":
    $url= "http://milinasweets.com'>";
     $title = "Милина Свитс";
        $description = "Новости за Милина Свтис.";
        $keywords    = "нови кондитоски производи, кондитоски производи.";
        $image = "fb.jpg";
        break;
        
        
    case "gallery":
    $url= "http://milinasweets.com'>";
     $title = "Милина Свитс";
        $description = "Слики од Милина Свтис и од нашите производи.";
        $keywords    = "Слики од компанијата и производи.";
        $image = "fb.jpg";
        break;
        
      case "contact":
       $title = "Милина Свитс";
       $url= "http://milinasweets.com'>";
        $description = "Контакт информаии на Милина Свист.";
        $keywords    = "нови кондитоски производи, кондитоски производи.";
        $image = "fb.jpg";
        break;  
      
      
      case "video":
      	$title = "Милина Свитс";
      	$url= "http://milinasweets.com'>";
        $description = "Видео галерија на Милина Свитс";
        $keywords    = "нови кондитоски производи, кондитоски производи.";
        $image = "fb.jpg";
        break;
    
    case "jobs":
        $url= "http://milinasweets.com/jobs'>";
        $title = "Работа во Милина Свитс - Штип";
        $description = "Конкурирај за едно од повеќето работни позиции во Милна Свитс - Штип";
        $keywords    = "Конкурирај за едно од повеќето работни позиции во Милна Свитс - Штип";
        $image = "fbjobs2.jpg";

        break;  
        
        
    default:
       // if all else fails...
       $url= "http://milinasweets.com'>";
       $title = "Милина Свитс";
       $description = "Компанија за приозвотство на кондиторскии производи во Штип, Македонија.";
       $keywords    = "milinasweets.com, milina, sweets, milina sweets, милина свитс, кондиторски производи, Штип";
       $image = "fb.jpg";
  }
 
  // output
 
  
  print "<meta name='description' content='".$description."' />\n";
  print "<meta name='keywords' content='".$keywords."' />\n";
  
  print "<meta property='og:title' content='".$title."'>\n";
  print "<meta property='og:locale' content='mk_MK'>\n";
  print "<meta property='og:url' content='".$url."\n";
  print "<meta property='og:site_name' content='Milina Sweets'>\n";
  print "<meta property='og:image' content='http://milinasweets.com/sites/all/themes/milina_sweets/images/".$image."'>\n";
  print "<meta property='og:image:type' content='image/jpeg'>\n";
  print "<meta property='og:image:width' content='484'\n";
  print "<meta property='og:image:height' content='252'>\n";
  
  }


}

function milina_sweets_preprocess_node(&$vars) {
    if ($vars['type'] == 'products') {
        $og_title = $vars['field_facebook_title']; 
        $og_title_single = ($og_title['und'][0][value]);
        $og_title = array( '#tag' => 'meta', '#attributes' => array( 'property' => 'og:title', 'content' => $og_title_single , ), ); 
        drupal_add_html_head($og_title , 'og_title');

        $og_descriptions = $vars['field_og_descriptions']; 
        $og_descriptions_single = ($og_descriptions ['und'][0][value]);
        $og_descriptions = array( '#tag' => 'meta', '#attributes' => array( 'property' => 'og:description', 'content' => $og_descriptions_single , ), ); 
        drupal_add_html_head($og_descriptions , 'og_descriptions ');

        $img = field_get_items('node', $vars['node'], 'field_facebook_image'); 
        $img_url = file_create_url($img[0]['uri']);
        $og_image = array( '#tag' => 'meta', '#attributes' => array( 'property' => 'og:image', 'content' => $img_url, ), ); 
        drupal_add_html_head($og_image, 'og_image');
    }
    
    if ($vars['type'] == 'news') {
        $og_title = $vars['field_news_facebook_title']; 
        $og_title_single = ($og_title['und'][0][value]);
        $og_title = array( '#tag' => 'meta', '#attributes' => array( 'property' => 'og:title', 'content' => $og_title_single , ), ); 
        drupal_add_html_head($og_title , 'og_title');

        $og_descriptions = $vars['field__news_og_descriptions']; 
        $og_descriptions_single = ($og_descriptions ['und'][0][value]);
        $og_descriptions = array( '#tag' => 'meta', '#attributes' => array( 'property' => 'og:description', 'content' => $og_descriptions_single , ), ); 
        drupal_add_html_head($og_descriptions , 'og_descriptions ');

        $img = field_get_items('node', $vars['node'], 'field__news_facebook_image'); 
        $img_url = file_create_url($img[0]['uri']);
        $og_image = array( '#tag' => 'meta', '#attributes' => array( 'property' => 'og:image', 'content' => $img_url, ), ); 
        drupal_add_html_head($og_image, 'og_image');
    }
    if ($vars['type'] == 'gallery') {
        $og_title = $vars['field_gallery_facebook_title']; 
        $og_title_single = ($og_title['und'][0][value]);
        $og_title = array( '#tag' => 'meta', '#attributes' => array( 'property' => 'og:title', 'content' => $og_title_single , ), ); 
        drupal_add_html_head($og_title , 'og_title');

        $og_descriptions = $vars['field_gallery_facebook_descripti']; 
        $og_descriptions_single = ($og_descriptions ['und'][0][value]);
        $og_descriptions = array( '#tag' => 'meta', '#attributes' => array( 'property' => 'og:description', 'content' => $og_descriptions_single , ), ); 
        drupal_add_html_head($og_descriptions , 'og_descriptions ');

        $img = field_get_items('node', $vars['node'], 'field_gallery_facebook_images'); 
        $img_url = file_create_url($img[0]['uri']);
        $og_image = array( '#tag' => 'meta', '#attributes' => array( 'property' => 'og:image', 'content' => $img_url, ), ); 
        drupal_add_html_head($og_image, 'og_image');
    }
    if ($vars['type'] == 'video') {
        $og_title = $vars['field_video_facebook_title']; 
        $og_title_single = ($og_title['und'][0][value]);
        $og_title = array( '#tag' => 'meta', '#attributes' => array( 'property' => 'og:title', 'content' => $og_title_single , ), ); 
        drupal_add_html_head($og_title , 'og_title');

        $og_descriptions = $vars['field_video_facebook_descripti']; 
        $og_descriptions_single = ($og_descriptions ['und'][0][value]);
        $og_descriptions = array( '#tag' => 'meta', '#attributes' => array( 'property' => 'og:description', 'content' => $og_descriptions_single , ), ); 
        drupal_add_html_head($og_descriptions , 'og_descriptions ');

        $img = field_get_items('node', $vars['node'], 'field_video_facebook_images'); 
        $img_url = file_create_url($img[0]['uri']);
        $og_image = array( '#tag' => 'meta', '#attributes' => array( 'property' => 'og:image', 'content' => $img_url, ), ); 
        drupal_add_html_head($og_image, 'og_image');
    }
}